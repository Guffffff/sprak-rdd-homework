package rdd.distcp

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, FileUtil, Path}
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

object distcp {


  def main(args: Array[String]): Unit = {

    var source = "/Users/guyuchen/Desktop/upload_file/"
    var target = "/Users/guyuchen/测试代码文件夹/"
    var partitionSize = 3
    var errorCatchFlag = true

    //-tp target  -sp source  -m partitionSize -i errorCatchFlag
    for(arg <- args) {
      if (arg.contains("-tp-")) {
        target = arg.replaceAll("-tp-", "").trim
      }
      if (arg.contains("-sp-")) {
        source = arg.replaceAll("-sp-", "").trim
      }
      if (arg.contains("-m-")) {
        val temp = arg.replaceAll("-m-", "").trim
        //todo 传参异常处理
        partitionSize = temp.toInt
      }
      if (arg.contains("-i-")) {
        val temp = arg.replaceAll("-i-", "").trim
        errorCatchFlag = temp.toBoolean
      }

    }
    val conf = new SparkConf().setAppName("distcp ").setMaster("local")
    val sc = new SparkContext(conf)
    val fileList = scala.collection.mutable.MutableList[(String, String)]()
    try {
      readAndBuildPath(source, target, fileList)
    } catch {
      case e: Exception => (
        if (errorCatchFlag) {
          println("创建文件夹时失败，异常为：" + e.getMessage, e)
          throw new Exception("创建文件夹时异常：")
        } else {
          println("创建文件夹时失败，继续")
        }
      )
    }

    try {
      val rdd = sc.makeRDD(fileList, partitionSize)
      rdd.mapPartitions(
        p => {
          while (p.hasNext) {
            val i = p.next()
            copyFile(i._1, i._2)
          }
          p
        }
      ).collect()
    } catch {
      case e: Exception => (
        if (errorCatchFlag) {
          println("copy文件时失败，异常为：" + e.getMessage, e)
          throw new Exception("copy文件时异常：")
        } else {
          println("copy文件时失败，继续")
        }
      )
    }
    println("distcp运行结束")

  }


  def readAndBuildPath(source: String, target: String, fileList: mutable.MutableList[(String, String)]): Unit = {

    val targetFileSystem = FileSystem.get(new Configuration())
    FileSystem
      .get(new Configuration())
      .listStatus(new Path(source))
      .foreach {
        p =>
          val sub = p.getPath.toString.split(source)(1)
          val buildingPath = target + sub
          println("创建" + buildingPath)
          if (p.isDirectory) {
            targetFileSystem.mkdirs(new Path(buildingPath))
            readAndBuildPath(p.getPath.toString, buildingPath, fileList)
          }
          else {
            fileList += (p.getPath.toString -> buildingPath)
          }
      }
    fileList
  }

  def copyFile(source: String, target: String): Boolean = {
    val fs = FileSystem.get(new Configuration())
    FileUtil.copy(fs, new Path(source), fs, new Path(target), false, new Configuration())
    true
  }


}
