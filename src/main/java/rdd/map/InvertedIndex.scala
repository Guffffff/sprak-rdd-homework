package rdd.map

import org.apache.spark.rdd.{ParallelCollectionRDD, RDD}
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source

object InvertedIndex {

  /**
   * 倒排索引1
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("invertedIndex ").setMaster("local")
    val sc = new SparkContext(conf)
    val source = Source.fromFile("src/main/resources/Inverted index.txt").getLines.toArray.map(x=>x.replaceAll("\"", ""))
    val baseData = sc.parallelize(source)
    homeworkOne(sc, baseData)
    homeworkTwo(sc, baseData)
  }


  def homeworkOne(sc : SparkContext, baseData : RDD[String] ):Unit = {
    println("homework-one")
    baseData.flatMap{
      lines =>
        val line = lines.split(",", -1)
        line(1).split(" ",-1).map((_, line(0)))
    }
      .distinct()
      .groupByKey()
      .foreach(x => println(s"${x._1}|${x._2.mkString(",")}"))
  }


  def homeworkTwo(sc : SparkContext, baseData : RDD[String]) : Unit = {
    println("homework-two")
    baseData.flatMap{
      lines =>
        val line = lines.split(",", -1)
        line(1).split(" ",-1).map(x => ((x, line(0)), 1))
    }
      .groupByKey()
      .map(
        x => (x._1._1, (x._1._2, x._2.sum))
      )
      .groupByKey()
      .foreach(x => println(s"${x._1}|${x._2.mkString(",")}"))
  }

}
